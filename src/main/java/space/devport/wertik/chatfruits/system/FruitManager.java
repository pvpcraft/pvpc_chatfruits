package space.devport.wertik.chatfruits.system;

import space.devport.utils.configutil.Configuration;
import space.devport.wertik.chatfruits.ChatFruits;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FruitManager {

    private List<String> fruits = new ArrayList<>();

    private List<String> bannedWords = new ArrayList<>();

    private final Configuration storage;

    private final Random random = new Random();

    public FruitManager(ChatFruits plugin) {
        this.storage = new Configuration(plugin, "data");
    }

    public void loadAll() {
        storage.reload();

        this.fruits = storage.getStringList("fruits", new ArrayList<>());
        this.bannedWords = storage.getStringList("banned-words", new ArrayList<>());
    }

    public void saveAll() {
        storage.getFileConfiguration().set("fruits", this.fruits);
        storage.getFileConfiguration().set("banned-words", this.bannedWords);

        storage.save();
    }

    public boolean addFruit(String fruit) {
        if (fruits.contains(fruit)) return false;
        fruits.add(fruit);
        return true;
    }

    public boolean removeFruit(String fruit) {
        return this.fruits.remove(fruit);
    }

    public String getFruit() {
        return this.getFruits().get(random.nextInt(this.getFruits().size()));
    }

    public boolean addWord(String word) {
        if (bannedWords.contains(word)) return false;
        bannedWords.add(word);
        return true;
    }

    public boolean removeWord(String word) {
        return this.bannedWords.remove(word);
    }

    public List<String> getFruits() {
        return Collections.unmodifiableList(this.fruits);
    }

    public List<String> getBannedWords() {
        return Collections.unmodifiableList(this.bannedWords);
    }
}