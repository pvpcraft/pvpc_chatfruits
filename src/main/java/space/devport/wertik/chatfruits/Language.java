package space.devport.wertik.chatfruits;

import lombok.Getter;
import org.bukkit.command.CommandSender;
import space.devport.utils.configutil.Configuration;
import space.devport.utils.messageutil.MessageBuilder;

import java.util.List;

public enum Language {

    HELP("Help", "&8&m        &r &bChat &dFruits &8&m        &r",
            "&d/%label% reload &8- &7Reload the plugin.",
            "&d/%label% words &8- &7List banned words.",
            "&d/%label% fruits &8- &7List exotic fruits.",
            "&d/%label% addFruit <name> &8- &7Add a fruit! Yummy!",
            "&d/%label% removeFruit <name> &8- &7Remove a fruit. :'(",
            "&d/%label% addWord <name> &8- &7Add a banned word.",
            "&d/%label% removeWord <name> &8- &7Remove a word."),

    NO_PERMS("No-Perms", "&cYou don't have permissions to do this."),
    TOO_MANY_ARGS("Too-Many-Arguments", "&cToo many arguments.", "&cUsage: &7%usage%"),
    NOT_ENOUGH_ARGS("Not-Enough-Arguments", "&cNot enough arguments.", "&cUsage: &7%usage%"),
    FRUIT_ALREADY_ADDED("Fruit-Already-Added", "&cFruit &f%fruit% &cis already added!"),
    WORD_ALREADY_ADDED("Word-Already-Added", "&cWord &f%word% &cis already added!"),

    WORD_LIST("Word-List", "&7Words: %words%"),
    FRUIT_LIST("Fruit-List", "&7Fruits: %fruits%"),
    FRUIT_ADDED("Fruit-Added", "&7Fruit &f%fruit% &7added."),
    WORD_ADDED("Word-Added", "&7Word &f%word% &7added."),
    FRUIT_REMOVED("Fruit-Removed", "&7Fruit &f%fruit% &7removed."),
    WORD_REMOVED("Word-Removed", "&7Word &f%word% &7removed."),
    FRUIT_NOT_SAVED("Fruit-Not-Saved", "&cFruit &f%fruit% &cis not on the list."),
    WORD_NOT_SAVED("Word-Not-Saved", "&cWord &f%word% &cis not on the list."),
    RELOAD("Reload", "&7Done... reload took &f%time%&7ms.")
    ;

    @Getter
    private final String path;

    @Getter
    private MessageBuilder value;

    Language(String path, String... value) {
        this.path = path;
        this.value = new MessageBuilder(value);
    }

    public void setValue(MessageBuilder value) {
        this.value = value;
    }

    public static void load() {
        Configuration lang = new Configuration(ChatFruits.getInstance(), "language");

        boolean save = false;

        for (Language message : values()) {
            if (!lang.getFileConfiguration().contains(message.getPath())) {
                lang.setMessageBuilder(message.getPath(), message.getValue());
                save = true;
            } else {
                message.setValue(lang.loadMessageBuilder(message.getPath(), message.getValue()));
            }
        }

        if (save)
            lang.save();
    }

    public void send(CommandSender sender) {
        if (!get().isEmptyAbsolute())
            get().send(sender);
    }

    public void sendPrefixed(CommandSender sender) {
        if (!getPrefixed().isEmpty())
            getPrefixed().send(sender);
    }

    public MessageBuilder get() {
        return new MessageBuilder(this.value);
    }

    public MessageBuilder getPrefixed() {
        if (get().isEmptyAbsolute())
            return new MessageBuilder();

        MessageBuilder message = new MessageBuilder(ChatFruits.getInstance().getConsoleOutput().getPrefix() + get().getWorkingMessage().get(0));
        List<String> subList = get().getWorkingMessage().subList(1, get().getWorkingMessage().size());

        if (!subList.isEmpty())
            subList.forEach(message::addLine);

        return message;
    }
}