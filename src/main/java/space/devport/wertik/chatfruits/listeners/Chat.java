package space.devport.wertik.chatfruits.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import space.devport.wertik.chatfruits.ChatFruits;
import space.devport.wertik.chatfruits.system.FruitManager;

public class Chat implements Listener {

    private final ChatFruits plugin;

    public Chat() {
        plugin = ChatFruits.getInstance();
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (!plugin.enabled ||
                plugin.getWorlds().contains(e.getPlayer().getWorld().getName()) ||
                plugin.ignoreOp && e.getPlayer().isOp() ||
                plugin.ignorePermission && e.getPlayer().hasPermission("chatfruits.ignore"))
            return;

        String message = e.getMessage();

        final FruitManager fruitManager = plugin.getFruitManager();

        for (String word : fruitManager.getBannedWords()) {
            message = message.replaceAll(word.toLowerCase(), fruitManager.getFruit().toLowerCase())
                    .replaceAll(word.toUpperCase(), fruitManager.getFruit().toUpperCase())
                    .replaceAll(word, fruitManager.getFruit()).replaceAll("(" + craftDotted(word) + ")", craftDotted(fruitManager.getFruit()))
                    .replaceAll("(" + craftDotted(word).toUpperCase() + ")", craftDotted(fruitManager.getFruit()).toUpperCase())
                    .replaceAll("(" + craftDotted(word).toLowerCase() + ")", craftDotted(fruitManager.getFruit()).toLowerCase())
                    .replaceAll("(?i)" + word, fruitManager.getFruit());
        }

        e.setMessage(message);
    }

    private String craftDotted(String str) {
        char[] chars = str.toCharArray();

        StringBuilder newString = new StringBuilder(String.valueOf(chars[0]));
        for (int i = 1; i < chars.length; i++) {
            newString.append(".").append(chars[i]);
        }

        return newString.toString();
    }
}