package space.devport.wertik.chatfruits.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import space.devport.wertik.chatfruits.ChatFruits;
import space.devport.wertik.chatfruits.Language;
import space.devport.wertik.chatfruits.system.FruitManager;
import space.devport.wertik.chatfruits.util.Utils;

public class ChatFruitsCommand implements CommandExecutor {

    private final ChatFruits plugin;

    public ChatFruitsCommand() {
        plugin = ChatFruits.getInstance();
    }

    private void help(CommandSender sender, String label) {
        Language.HELP.get().fill("%label%", label).send(sender);
    }

    private boolean notEnough(CommandSender sender, String usage) {
        Language.NOT_ENOUGH_ARGS.getPrefixed().fill("%usage%", usage).send(sender);
        return true;
    }

    private boolean tooMany(CommandSender sender, String usage) {
        Language.TOO_MANY_ARGS.getPrefixed().fill("%usage%", usage).send(sender);
        return true;
    }

    @Override
    public boolean onCommand(CommandSender sender, @NotNull Command cmd, @NotNull String label, String[] args) {
        if (!sender.hasPermission("chatfruits.admin")) {
            Language.NO_PERMS.sendPrefixed(sender);
            return true;
        }

        if (args.length == 0) {
            help(sender, label);
            return true;
        }

        final FruitManager fruitManager = plugin.getFruitManager();

        switch (args[0].toLowerCase()) {
            case "reload":
                plugin.reload(sender);
                break;
            case "words":
                Language.WORD_LIST.getPrefixed()
                        .fill("%words%", Utils.listToString(fruitManager.getBannedWords(), "&f, &7", "&cNo words configured."))
                        .send(sender);
                break;
            case "fruits":
                Language.FRUIT_LIST.getPrefixed()
                        .fill("%fruits%", Utils.listToString(fruitManager.getFruits(), "&f, &7", "&cNo fruits configured."))
                        .send(sender);
                break;
            case "addfruit":
                if (args.length > 2)
                    return tooMany(sender, "/" + label + " addFruit <fruit>");
                else if (args.length < 2)
                    return notEnough(sender, "/" + label + " addFruit <fruit>");

                if (fruitManager.addFruit(args[1]))
                    Language.FRUIT_ADDED.getPrefixed()
                            .fill("%fruit%", args[1])
                            .send(sender);
                else
                    Language.FRUIT_ALREADY_ADDED.getPrefixed()
                            .fill("%fruit%", args[1])
                            .send(sender);
                break;
            case "addword":
                if (args.length > 2)
                    return tooMany(sender, "/" + label + " addWord <word>");
                else if (args.length < 2)
                    return notEnough(sender, "/" + label + " addWord <word>");

                if (fruitManager.addWord(args[1]))
                    Language.WORD_ADDED.getPrefixed()
                            .fill("%word%", args[1])
                            .send(sender);
                else
                    Language.WORD_ALREADY_ADDED.getPrefixed()
                            .fill("%word%", args[1])
                            .send(sender);
                break;
            case "removefruit":
                if (args.length > 2)
                    return tooMany(sender, "/" + label + " removeFruit <fruit>");
                else if (args.length < 2)
                    return notEnough(sender, "/" + label + " removeFruit <fruit>");

                if (fruitManager.removeFruit(args[1]))
                    Language.FRUIT_REMOVED.getPrefixed()
                            .fill("%fruit%", args[1])
                            .send(sender);
                else
                    Language.FRUIT_NOT_SAVED.getPrefixed()
                            .fill("%fruit%", args[1])
                            .send(sender);
                break;
            case "removeword":
                if (args.length > 2)
                    return tooMany(sender, "/" + label + " removeWord <word>");
                else if (args.length < 2)
                    return notEnough(sender, "/" + label + " removeWord <word>");

                if (fruitManager.removeWord(args[1]))
                    Language.WORD_REMOVED.getPrefixed()
                            .fill("%word%", args[1])
                            .send(sender);
                else
                    Language.WORD_NOT_SAVED.getPrefixed()
                            .fill("%word%", args[1])
                            .send(sender);
                break;
            default:
                help(sender, label);
        }

        return true;
    }
}