package space.devport.wertik.chatfruits;

import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import space.devport.utils.ConsoleOutput;
import space.devport.utils.DevportUtils;
import space.devport.utils.configutil.Configuration;
import space.devport.wertik.chatfruits.commands.ChatFruitsCommand;
import space.devport.wertik.chatfruits.listeners.Chat;
import space.devport.wertik.chatfruits.system.FruitManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChatFruits extends JavaPlugin {

    @Getter
    private static ChatFruits instance;

    @Getter
    public Random random;

    @Getter
    public ConsoleOutput consoleOutput;

    @Getter
    private Configuration configuration;

    @Getter
    private List<String> worlds;

    @Getter
    private FruitManager fruitManager;

    public boolean ignorePermission;
    public boolean ignoreOp;
    public boolean save;
    public boolean enabled;

    @Override
    public void onEnable() {
        instance = this;

        DevportUtils utils = new DevportUtils(this);
        consoleOutput = utils.getConsoleOutput();
        consoleOutput.setUseBukkit(true);

        configuration = new Configuration(this, "config");

        random = new Random();

        loadSettings();
        Language.load();

        fruitManager = new FruitManager(this);
        fruitManager.loadAll();

        getServer().getPluginManager().registerEvents(new Chat(), this);

        getCommand("chatfruits").setExecutor(new ChatFruitsCommand());
    }

    public void reload(CommandSender sender) {
        long start = System.currentTimeMillis();

        consoleOutput.addListener(sender);

        consoleOutput.info("Reloading..");

        fruitManager.saveAll();

        configuration.reload();

        loadSettings();
        Language.load();

        fruitManager.loadAll();

        consoleOutput.info("Loaded " + fruitManager.getBannedWords().size() + " banned word(s) and " + fruitManager.getFruits().size() + " fruit(s)..");

        consoleOutput.removeListener(sender);

        Language.RELOAD.getPrefixed().fill("%time%", "" + (System.currentTimeMillis() - start)).send(sender);
    }

    private void loadSettings() {
        consoleOutput.setDebug(configuration.getFileConfiguration().getBoolean("debug-enabled", false));
        consoleOutput.setPrefix(configuration.getColoredString("prefix", ""));

        worlds = configuration.getStringList("disabled-worlds", new ArrayList<>());

        ignoreOp = configuration.getFileConfiguration().getBoolean("ignore-op", true);
        ignorePermission = configuration.getFileConfiguration().getBoolean("ignore-permission", true);
        save = configuration.getFileConfiguration().getBoolean("save-on-disable", false);
        enabled = configuration.getFileConfiguration().getBoolean("enabled", true);
    }

    @Override
    public void onDisable() {
        fruitManager.saveAll();
    }

    @Override
    @NotNull
    public FileConfiguration getConfig() {
        return configuration.getFileConfiguration();
    }

    @Override
    public void saveConfig() {
        configuration.save();
    }
}