package space.devport.wertik.chatfruits.util;

import java.util.List;

public class Utils {

    public static String listToString(List<String> list, String splitter, String ifEmpty) {
        StringBuilder stringList = ifEmpty == null ? new StringBuilder("") : new StringBuilder(ifEmpty);

        if (list != null && !list.isEmpty()) {
            stringList = new StringBuilder(list.get(0));

            for (int i = 1; i < list.size(); i++)
                stringList.insert(0, list.get(i) + splitter);
        }

        return stringList.toString();
    }
}